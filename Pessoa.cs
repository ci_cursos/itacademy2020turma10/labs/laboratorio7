using System;
using System.Diagnostics.CodeAnalysis;

namespace laboratorio7
{
    public class Pessoa : IComparable<Pessoa>
    {
        public string Nome {get;set;}
        public int Idade {get;set;}

        public int CompareTo(Pessoa other)
        {
            if(other == null)
            {
                return 1;
            }
            return Nome.CompareTo(other.Nome);
        }
    }
}