﻿using System;

namespace laboratorio7
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            string[] lista = { "Luana", "Lana", "Júlio", "Jana", "Lúcia", "Daniel", "João", "á", "a","b","julio" };
            Console.WriteLine("Array antes da ordenacao");
            foreach (var item in lista)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            Array.Sort(lista);
            Console.WriteLine("Array depois da ordenacao");
            foreach (var item in lista)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            */
            Pessoa p1 = new Pessoa{Nome="John", Idade=23};
            Pessoa p2 = new Pessoa{Nome="Maria", Idade=20};
            Pessoa p3 = new Pessoa{Nome="Cláudia", Idade=18};
            Pessoa[] pessoas = {p1, p2, p3};
            Array.Sort(pessoas);
        }
    }
}
